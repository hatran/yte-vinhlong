var dataptramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Phường Cái Vồn",
   "address": "Phường Cái Vồn, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0665624,
   "Latitude": 105.8084823
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Phường Thành Phước",
   "address": "Phường Thành Phước, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0610769,
   "Latitude": 105.7999505
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Xã Thuận An",
   "address": "Xã Thuận An, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0902432,
   "Latitude": 105.8231588
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Xã Đông Thạnh",
   "address": "Xã Đông Thạnh, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0120838,
   "Latitude": 105.8701316
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Xã Đông Bình",
   "address": "Xã Đông Bình, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.056571,
   "Latitude": 105.8466437
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Phường Đông Thuận",
   "address": "Phường Đông Thuận, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0451829,
   "Latitude": 105.8361807
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Xã Mỹ Hòa",
   "address": "Xã Mỹ Hòa, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0137967,
   "Latitude": 105.8349009
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Xã Đông Thành",
   "address": "Xã Đông Thành, Thị xã Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0120838,
   "Latitude": 105.8701316
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Phường 9",
   "address": "Phường 9, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2549168,
   "Latitude": 105.9494248
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Phường 5",
   "address": "Phường 5, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2528551,
   "Latitude": 105.9905529
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2551704,
   "Latitude": 105.9714566
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2474681,
   "Latitude": 105.9626436
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Phường 4",
   "address": "Phường 4, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2410092,
   "Latitude": 105.983208
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Phường 3",
   "address": "Phường 3, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2374531,
   "Latitude": 105.9729255
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Phường 8",
   "address": "Phường 8, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2329064,
   "Latitude": 105.9552997
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Xã Tân Ngãi",
   "address": "Xã Tân Ngãi, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2615231,
   "Latitude": 105.9259271
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Xã Tân Hòa",
   "address": "Xã Tân Hòa, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2515421,
   "Latitude": 105.9083056
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Xã Tân Hội",
   "address": "Xã Tân Hội, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2604215,
   "Latitude": 105.8936224
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Xã Trường An",
   "address": "Xã Trường An, Thành phố Vĩnh Long, Tỉnh Vĩnh Long",
   "Longtitude": 10.2503685,
   "Latitude": 105.9318012
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Thị trấn Trà Ôn",
   "address": "Thị trấn Trà Ôn, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.96335109999999,
   "Latitude": 105.9259271
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Xã Xuân Hiệp",
   "address": "Xã Xuân Hiệp, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 10.0678409,
   "Latitude": 106.0581397
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Xã Nhơn Bình",
   "address": "Xã Nhơn Bình, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 10.0479233,
   "Latitude": 106.022874
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Xã Hòa Bình",
   "address": "Xã Hòa Bình, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 10.035354,
   "Latitude": 106.0581397
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Xã Thới Hòa",
   "address": "Xã Thới Hòa, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 10.0028797,
   "Latitude": 106.0581397
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Xã Trà Côn",
   "address": "Xã Trà Côn, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 10.0051915,
   "Latitude": 106.0111203
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Xã Tân Mỹ",
   "address": "Xã Tân Mỹ, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9852697,
   "Latitude": 105.9758633
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Xã Hựu Thành",
   "address": "Xã Hựu Thành, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9823916,
   "Latitude": 106.0346285
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Xã Vĩnh Xuân",
   "address": "Xã Vĩnh Xuân, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9510837,
   "Latitude": 106.0111203
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Xã Thuận Thới",
   "address": "Xã Thuận Thới, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9493595,
   "Latitude": 106.0463837
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Xã Phú Thành",
   "address": "Xã Phú Thành, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.96819659999999,
   "Latitude": 105.8818766
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Xã Thiện Mỹ",
   "address": "Xã Thiện Mỹ, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9539484,
   "Latitude": 105.9523622
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Xã Lục Sỹ Thành",
   "address": "Xã Lục Sỹ Thành, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9448397,
   "Latitude": 105.9171161
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Xã Tích Thiện",
   "address": "Xã Tích Thiện, Huyện Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.91977949999999,
   "Latitude": 105.9876149
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Thị trấn Tam Bình",
   "address": "Thị trấn Tam Bình, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0478007,
   "Latitude": 105.9978981
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Xã Tân Lộc",
   "address": "Xã Tân Lộc, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.150645,
   "Latitude": 105.9729255
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Xã Phú Thịnh",
   "address": "Xã Phú Thịnh, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1307443,
   "Latitude": 105.8818766
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Xã Hậu Lộc",
   "address": "Xã Hậu Lộc, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1255182,
   "Latitude": 105.9876149
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Xã Hòa Thạnh",
   "address": "Xã Hòa Thạnh, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1225988,
   "Latitude": 106.0463837
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Xã Hoà Lộc",
   "address": "Xã Hoà Lộc, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1026738,
   "Latitude": 106.0111203
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Xã Phú Lộc",
   "address": "Xã Phú Lộc, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1170021,
   "Latitude": 105.9406128
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Xã Song Phú",
   "address": "Xã Song Phú, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1073188,
   "Latitude": 105.9171161
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Xã Hòa Hiệp",
   "address": "Xã Hòa Hiệp, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.101508,
   "Latitude": 106.0346285
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Xã Mỹ Lộc",
   "address": "Xã Mỹ Lộc, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0839018,
   "Latitude": 105.9523622
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Xã Tân Phú",
   "address": "Xã Tân Phú, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0765268,
   "Latitude": 105.8818766
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Xã Long Phú",
   "address": "Xã Long Phú, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0747978,
   "Latitude": 105.9171161
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Xã Mỹ Thạnh Trung",
   "address": "Xã Mỹ Thạnh Trung, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0610725,
   "Latitude": 105.9758633
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Xã Tường Lộc",
   "address": "Xã Tường Lộc, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0570612,
   "Latitude": 106.0023054
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã Loan Mỹ",
   "address": "Xã Loan Mỹ, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0291539,
   "Latitude": 105.9641124
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Ngãi Tứ",
   "address": "Xã Ngãi Tứ, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0103668,
   "Latitude": 105.9053689
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Xã Bình Ninh",
   "address": "Xã Bình Ninh, Huyện Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0143475,
   "Latitude": 105.9347384
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Thị trấn Cái Nhum",
   "address": "Thị trấn Cái Nhum, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.1775604,
   "Latitude": 106.1125201
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Xã Mỹ An",
   "address": "Xã Mỹ An, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.2310542,
   "Latitude": 106.0463837
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Xã Mỹ Phước",
   "address": "Xã Mỹ Phước, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.2286865,
   "Latitude": 106.0934117
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã An Phước",
   "address": "Xã An Phước, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.2052122,
   "Latitude": 106.1286901
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Xã Nhơn Phú",
   "address": "Xã Nhơn Phú, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.2075806,
   "Latitude": 106.0816536
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Xã Long Mỹ",
   "address": "Xã Long Mỹ, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.2191112,
   "Latitude": 106.0140586
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Xã Hòa Tịnh",
   "address": "Xã Hòa Tịnh, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.2093521,
   "Latitude": 106.0463837
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Xã Chánh Hội",
   "address": "Xã Chánh Hội, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.1852974,
   "Latitude": 106.0934117
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Xã Bình Phước",
   "address": "Xã Bình Phước, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.181938,
   "Latitude": 106.0522616
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Xã Chánh An",
   "address": "Xã Chánh An, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.1714987,
   "Latitude": 106.1522126
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Xã Tân An Hội",
   "address": "Xã Tân An Hội, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.15277,
   "Latitude": 106.0934117
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Xã Tân Long",
   "address": "Xã Tân Long, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.1551209,
   "Latitude": 106.0463837
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Tân Long Hội",
   "address": "Xã Tân Long Hội, Huyện Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.132266,
   "Latitude": 106.0698963
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Thị trấn Long Hồ",
   "address": "Thị trấn Long Hồ, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1962734,
   "Latitude": 106.0096511
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Đồng Phú",
   "address": "Xã Đồng Phú, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.3100252,
   "Latitude": 105.9876149
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Xã Bình Hòa Phước",
   "address": "Xã Bình Hòa Phước, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.2785238,
   "Latitude": 106.0199355
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Xã Hòa Ninh",
   "address": "Xã Hòa Ninh, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.2857302,
   "Latitude": 105.9846769
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Xã An Bình",
   "address": "Xã An Bình, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.2792074,
   "Latitude": 105.9523622
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Xã Thanh Đức",
   "address": "Xã Thanh Đức, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.2436801,
   "Latitude": 106.0111203
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Xã Tân Hạnh",
   "address": "Xã Tân Hạnh, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.2152272,
   "Latitude": 105.9288641
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Xã Phước Hậu",
   "address": "Xã Phước Hậu, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.2109026,
   "Latitude": 105.9611748
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Xã Long Phước",
   "address": "Xã Long Phước, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.2122954,
   "Latitude": 105.9876149
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Xã Phú Đức",
   "address": "Xã Phú Đức, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1797435,
   "Latitude": 105.9876149
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Xã Lộc Hòa",
   "address": "Xã Lộc Hòa, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1935186,
   "Latitude": 105.9288641
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Xã Long An",
   "address": "Xã Long An, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1568793,
   "Latitude": 106.0111203
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Xã Phú Quới",
   "address": "Xã Phú Quới, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1508104,
   "Latitude": 105.922551
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Xã Thạnh Quới",
   "address": "Xã Thạnh Quới, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.173562,
   "Latitude": 105.8936224
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Xã Hòa Phú",
   "address": "Xã Hòa Phú, Huyện Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1603837,
   "Latitude": 105.9406128
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Xã Tân Hưng",
   "address": "Xã Tân Hưng, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1816577,
   "Latitude": 105.7292491
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Xã Tân Thành",
   "address": "Xã Tân Thành, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1582164,
   "Latitude": 105.7644596
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Xã Thành Trung",
   "address": "Xã Thành Trung, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1462112,
   "Latitude": 105.7879371
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Xã Tân An Thạnh",
   "address": "Xã Tân An Thạnh, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1399441,
   "Latitude": 105.6940454
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Xã Tân Lược",
   "address": "Xã Tân Lược, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1490834,
   "Latitude": 105.7292491
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Xã Nguyễn Văn Thảnh",
   "address": "Xã Nguyễn Văn Thảnh, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1330552,
   "Latitude": 105.8349009
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Xã Thành Đông",
   "address": "Xã Thành Đông, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1227995,
   "Latitude": 105.7673942
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Xã Mỹ Thuận",
   "address": "Xã Mỹ Thuận, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1119347,
   "Latitude": 105.8231588
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Xã Tân Bình",
   "address": "Xã Tân Bình, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.1165215,
   "Latitude": 105.7292491
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Xã Thành Lợi",
   "address": "Xã Thành Lợi, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.0919631,
   "Latitude": 105.7879371
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Xã Tân Quới",
   "address": "Xã Tân Quới, Huyện Bình Tân, Tỉnh Vĩnh Long",
   "Longtitude": 10.0962481,
   "Latitude": 105.7556564
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Thị trấn Vũng Liêm",
   "address": "Thị trấn Vũng Liêm, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0910316,
   "Latitude": 106.1904426
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Xã Tân Quới Trung",
   "address": "Xã Tân Quới Trung, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.1515919,
   "Latitude": 106.1169299
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Xã Quới Thiện",
   "address": "Xã Quới Thiện, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.1474539,
   "Latitude": 106.199266
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Xã Quới An",
   "address": "Xã Quới An, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.138394,
   "Latitude": 106.1639749
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Xã Trung Chánh",
   "address": "Xã Trung Chánh, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.1293277,
   "Latitude": 106.1286901
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Xã Tân An Luông",
   "address": "Xã Tân An Luông, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.1094197,
   "Latitude": 106.0934117
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Xã Thanh Bình",
   "address": "Xã Thanh Bình, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.1023523,
   "Latitude": 106.2345633
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Xã Trung Thành Tây",
   "address": "Xã Trung Thành Tây, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.115546,
   "Latitude": 106.1875016
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Xã Trung Hiệp",
   "address": "Xã Trung Hiệp, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0956518,
   "Latitude": 106.1522126
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Xã Hiếu Phụng",
   "address": "Xã Hiếu Phụng, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0865819,
   "Latitude": 106.1169299
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Xã Trung Thành Đông",
   "address": "Xã Trung Thành Đông, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0812902,
   "Latitude": 106.2227968
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Xã Trung Thành",
   "address": "Xã Trung Thành, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0614065,
   "Latitude": 106.1875016
 },
 {
   "STT": 102,
   "Name": "Trạm y tế Xã Trung Hiếu",
   "address": "Xã Trung Hiếu, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0688732,
   "Latitude": 106.1463317
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Xã Trung Ngãi",
   "address": "Xã Trung Ngãi, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0474369,
   "Latitude": 106.1963248
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Hiếu Thuận",
   "address": "Xã Hiếu Thuận, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0540956,
   "Latitude": 106.1169299
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Xã Trung Nghĩa",
   "address": "Xã Trung Nghĩa, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0277674,
   "Latitude": 106.2110311
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Xã Trung An",
   "address": "Xã Trung An, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0306944,
   "Latitude": 106.1522126
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Xã Hiếu Nhơn",
   "address": "Xã Hiếu Nhơn, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.021622,
   "Latitude": 106.1169299
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Xã Hiếu Thành",
   "address": "Xã Hiếu Thành, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 9.9897411,
   "Latitude": 106.1051705
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Xã Hiếu Nghĩa",
   "address": "Xã Hiếu Nghĩa, Huyện  Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 9.9686848,
   "Latitude": 106.0934117
 }
];