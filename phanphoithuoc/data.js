var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện Đa khoa tỉnh Vĩnh Long",
   "address": "301, Trần Phú, Phường 4, thành phố Vĩnh Long, tỉnh Vĩnh Long",
   "Longtitude": 10232454,
   "Latitude": 105.988411
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Đa khoa huyện Bình Minh",
   "address": "Ấp Thuận Thới, Xã Thuận An, huyện Bình Minh, tỉnh Vĩnh Long",
   "Longtitude": 10.071653,
   "Latitude": 105.823844
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Đa khoa huyện Long Hồ",
   "address": "Thị Trấn Long Hồ, huyện Long Hồ, tỉnh Vĩnh Long",
   "Longtitude": 10.192099,
   "Latitude": 106.011196
 },
 {
   "STT": 4,
   "Name": "Bệnh viện Đa khoa huyện Mang Thít",
   "address": "Nguyễn Trung Trực, Khóm 4, Thị Trấn Cái Nhum, huyện Mang Thít, tỉnh Vĩnh Long",
   "Longtitude": 10.175688,
   "Latitude": 106.109535
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Đa khoa huyện Tam Bình",
   "address": "Khóm 4, Thị Trấn Tam Bình, huyện Tam Bình, tỉnh Vĩnh Long",
   "Longtitude": 10.043615,
   "Latitude": 105.995583
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Đa khoa huyện Trà Ôn",
   "address": "32/7A Đường Võ Tánh, Thị Trấn Trà Ôn, huyện Trà Ôn, tỉnh Vĩnh Long",
   "Longtitude": 9.9694846,
   "Latitude": 105.9196079
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Y dược cổ truyền Vĩnh Long",
   "address": "37 Trần Phú, T p Vĩnh Long, tỉnh Vĩnh Long",
   "Longtitude": 10.2510735,
   "Latitude": 105.9763741
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Đa khoa Nguyễn Văn Thủ",
   "address": "Số 73, Đường Nam Kỳ Khởi Nghĩa, Khóm I, Thị trấn Vũng Liêm, Huyện Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.09265,
   "Latitude": 106.186164
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Đa khoa khu vực quân dân y kết hợp Bình Minh",
   "address": "Tân Thành, huyện Bình Tân, Vĩnh Long",
   "Longtitude": 10.2565928,
   "Latitude": 105.9628159
 },
 {
   "STT": 10,
   "Name": "Trung tâm Y tế thành phố Vĩnh Long",
   "address": "18 Ngô Quyền, Phường 2, Vĩnh Long,",
   "Longtitude": 10.2558962,
   "Latitude": 105.9679451
 },
 {
   "STT": 11,
   "Name": "Trung tâm Y tế huyện Mang Thít",
   "address": "2/55 Hùng Vương, Cái Nhum, Mang Thít, Vĩnh Long",
   "Longtitude": 10.17641,
   "Latitude": 106.109432
 },
 {
   "STT": 12,
   "Name": "Trung tâm Y tế huyện Long Hồ",
   "address": "Khóm 1, Thị Trấn Long Hồ, Huyện Long Hồ, Long Hồ, Vĩnh Long",
   "Longtitude": 10.192536,
   "Latitude": 106.010889
 },
 {
   "STT": 13,
   "Name": "Trung tâm Y tế huyện Vũng Liêm",
   "address": "Khóm 1, Thị Trấn Vũng Liêm, Huyện Vũng Liêm, Vũng Liêm, Vĩnh Long",
   "Longtitude": 10.09103,
   "Latitude": 106.190442
 },
 {
   "STT": 14,
   "Name": "Trung tâm Y tế huyện Tam Bình",
   "address": "Khóm 4, Thị Trấn Tam Bình, Huyện Tam Bình, Vĩnh Long",
   "Longtitude": 10.047803,
   "Latitude": 105.997898
 },
 {
   "STT": 15,
   "Name": "Trung tâm Y tế thị xã Bình Minh",
   "address": "Quốc Lộ 1A, Thuận An, Bình Minh, Vĩnh Long",
   "Longtitude": 10.07045,
   "Latitude": 105.824714
 },
 {
   "STT": 16,
   "Name": "Trung tâm Y tế huyện Trà Ôn",
   "address": "Khu 7, Trà Ôn, Vĩnh Long",
   "Longtitude": 9.969825,
   "Latitude": 105.924324
 },
 {
   "STT": 17,
   "Name": "Trung tâm Y tế huyện Bình Tân",
   "address": "Tổ 3, Ấp Tân Lợi, Xã Tân Quới, Huyện Bình Tân, Vĩnh Long",
   "Longtitude": 10.096256,
   "Latitude": 105.755654
 }
];