var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Nhà thuốc Khải Hoàn",
   "address": "79 , Đường 1/5 ,  thành phố  Vĩnh Long",
   "Longtitude": 10.2688361,
   "Latitude": 105.9102952
 },
 {
   "STT": 2,
   "Name": "Nhà thuốc Ngọc Lan",
   "address": "64/4C Trần Phú ,  thành phố  Vĩnh Long",
   "Longtitude": 10.251497,
   "Latitude": 105.9779951
 },
 {
   "STT": 3,
   "Name": "Nhà thuốc Hồng Huệ",
   "address": "18 , đường 1/5 ,  thành phố  Vĩnh Long",
   "Longtitude": 10.2259191,
   "Latitude": 105.9349072
 },
 {
   "STT": 4,
   "Name": "Nhà thuốc Ngọc Thành",
   "address": "67/3 ,  Phạm Thái Bường ,  thành phố  Vĩnh Long",
   "Longtitude": 10.249529,
   "Latitude": 105.9745589
 },
 {
   "STT": 5,
   "Name": "Nhà thuốc  Đức Thọ Sanh",
   "address": "26, đường 1/5, thành phố  Vĩnh Long",
   "Longtitude": 10.2663842,
   "Latitude": 105.9219222
 },
 {
   "STT": 6,
   "Name": "Nhà thuốc Hạnh Phúc",
   "address": "Phạm Hùng , P9, thành phố  Vĩnh Long",
   "Longtitude": 10.2594784,
   "Latitude": 105.9458963
 },
 {
   "STT": 7,
   "Name": "Nhà thuốc Thùy Dương",
   "address": "4 ,  Chi Lăng, thành phố  Vĩnh Long",
   "Longtitude": 10.2571177,
   "Latitude": 105.9730428
 },
 {
   "STT": 8,
   "Name": "Nhà thuốc Thiên Tân 2",
   "address": "78 , Trần Phú , P 4, thành phố  Vĩnh Long",
   "Longtitude": 10.2303754,
   "Latitude": 105.9920484
 },
 {
   "STT": 9,
   "Name": "Nhà thuốc Thiên Tân",
   "address": "5 ,  Đinh Tiên Hoàng, thành phố  Vĩnh Long",
   "Longtitude": 10.2396886,
   "Latitude": 105.9572055
 },
 {
   "STT": 10,
   "Name": "Nhà thuốc Thanh Hiền 1",
   "address": "Cao Thắng , P 8 , thành phố  Vĩnh Long",
   "Longtitude": 10.2370022,
   "Latitude": 105.959157
 },
 {
   "STT": 11,
   "Name": "Nhà thuốc Phúc Linh",
   "address": "Phạm Hùng, P 9, thành phố  Vĩnh Long",
   "Longtitude": 10.2594784,
   "Latitude": 105.9458963
 },
 {
   "STT": 12,
   "Name": "Nhà thuốc Minh Tâm",
   "address": "16,  Cao Thắng, thành phố  Vĩnh Long",
   "Longtitude": 10.2362643,
   "Latitude": 105.9589722
 },
 {
   "STT": 13,
   "Name": "Nhà thuốc Nhơn Hòa",
   "address": "10 ,  Chi Lăng, thành phố  Vĩnh Long",
   "Longtitude": 10.2571083,
   "Latitude": 105.9730521
 },
 {
   "STT": 14,
   "Name": "Nhà thuốc Lập Thành",
   "address": "45 đường 30/4, thành phố  Vĩnh Long",
   "Longtitude": 10.239574,
   "Latitude": 105.9571928
 },
 {
   "STT": 15,
   "Name": "Nhà thuốc Mai Khanh",
   "address": "29 đường 3/2, thành phố  Vĩnh Long",
   "Longtitude": 10.2547641,
   "Latitude": 105.9713877
 },
 {
   "STT": 16,
   "Name": "Nhà thuốc Hồng yến",
   "address": "21H Phạm Thái Bường, P4, thành phố  Vĩnh Long",
   "Longtitude": 10.2490356,
   "Latitude": 105.9752325
 },
 {
   "STT": 18,
   "Name": "Nhà thuốc Thiên Thọ Đường",
   "address": "35 đường 1/5, Phường 1, thành phố  Vĩnh Long",
   "Longtitude": 10.2569585,
   "Latitude": 105.9735895
 },
 {
   "STT": 19,
   "Name": "Nhà thuốc Khải Hoàn",
   "address": "05 Lưu Văn Việt, thành phố  Vĩnh Long",
   "Longtitude": 10.2544044,
   "Latitude": 105.9657526
 },
 {
   "STT": 20,
   "Name": "Nhà thuốc Bình Nhu",
   "address": "110 Đinh Tiên Hoàng, thành phố  Vĩnh Long",
   "Longtitude": 10.2229899,
   "Latitude": 105.9523707
 },
 {
   "STT": 21,
   "Name": "Nhà thuốc Anh Huy",
   "address": "F31 Đinh Tiên Hoàng,  thành phố  Vĩnh Long",
   "Longtitude": 10.2299525,
   "Latitude": 105.9554116
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc Hoa- Hoa",
   "address": "Khóm 1 , Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1962734,
   "Latitude": 106.0096511
 },
 {
   "STT": 23,
   "Name": "Nhà thuốc Xuân Hồng",
   "address": "Khóm 1, TT Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1962734,
   "Latitude": 106.0096511
 },
 {
   "STT": 24,
   "Name": "Nhà thuốc Bội Bội-Bội",
   "address": "Hòa Phú , Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1603837,
   "Latitude": 105.9406128
 },
 {
   "STT": 25,
   "Name": "Nhà thuốc Yến Ly",
   "address": "Hòa Phú , Long Hồ, Tỉnh Vĩnh Long",
   "Longtitude": 10.1603837,
   "Latitude": 105.9406128
 },
 {
   "STT": 26,
   "Name": "Nhà thuốc Cây Công",
   "address": "830 Ngô Quyền, Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0680229,
   "Latitude": 105.8207533
 },
 {
   "STT": 27,
   "Name": "Nhà thuốc Ngọc Chi",
   "address": "Ngô Quyền , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0720204,
   "Latitude": 105.8205335
 },
 {
   "STT": 28,
   "Name": "Nhà thuốc Hồng Phước",
   "address": "20,  Chợ cái Vồn ,  Bình Minh, Tỉnh Vĩnh Long",
   "Longtitude": 10.0902432,
   "Latitude": 105.8231588
 },
 {
   "STT": 29,
   "Name": "Nhà thuốc Thanh Loan",
   "address": "Song Phú ,  Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1181619,
   "Latitude": 105.9171161
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Huy Khang",
   "address": " Cái Ngang ,  Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.1031348,
   "Latitude": 105.9596211
 },
 {
   "STT": 31,
   "Name": "Nhà thuốc Diễm Nhi",
   "address": "Tổ 16, Mỹ Phú ,  Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.084199,
   "Latitude": 105.9526192
 },
 {
   "STT": 32,
   "Name": "Nhà thuốc Lưu Bá Nghĩa",
   "address": "Khóm 1 ,  Tam Bình, Tỉnh Vĩnh Long",
   "Longtitude": 10.0478007,
   "Latitude": 105.9978981
 },
 {
   "STT": 33,
   "Name": "Nhà thuốc Phú Lộc.",
   "address": "14,tổ 1,ấp Phú Hòa Yên, xã Song Phú, Tỉnh Vĩnh Long",
   "Longtitude": 10.1045586,
   "Latitude": 105.8925806
 },
 {
   "STT": 34,
   "Name": "Nhà thuốc Huy Khang 2",
   "address": "Ấp Phú Mỹ, Xã Mỹ Lộc, Tỉnh Vĩnh Long",
   "Longtitude": 10.0840497,
   "Latitude": 105.9537355
 },
 {
   "STT": 35,
   "Name": "Nhà thuốc Trường Thọ",
   "address": "25C Gia Long, Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9660541,
   "Latitude": 105.9206536
 },
 {
   "STT": 36,
   "Name": "Nhà thuốc Liên Ngọc",
   "address": "42b ,  Gia Long , Trà Ôn, Tỉnh Vĩnh Long",
   "Longtitude": 9.9660134,
   "Latitude": 105.9197366
 },
 {
   "STT": 37,
   "Name": "Nhà thuốc Huỳnh Bính",
   "address": "138 ,  k1 ,  Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.18076,
   "Latitude": 106.0757749
 },
 {
   "STT": 38,
   "Name": "Nhà thuốc Mai Ly",
   "address": "Khóm 1 Thị Trấn Mang Thít, Tỉnh Vĩnh Long",
   "Longtitude": 10.1740716,
   "Latitude": 106.1095335
 },
 {
   "STT": 39,
   "Name": "Nhà thuốc PHƯỚC THỌ SANH",
   "address": "Chợ Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0966345,
   "Latitude": 106.1858408
 },
 {
   "STT": 40,
   "Name": "Nhà thuốc Thai Nhung",
   "address": "Chợ Hiếu Phụng ,  Huyện Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0833411,
   "Latitude": 106.1180568
 },
 {
   "STT": 41,
   "Name": "Nhà thuốc Vĩnh Bình",
   "address": "Khu phố 1 Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0940462,
   "Latitude": 106.1861229
 },
 {
   "STT": 42,
   "Name": "Nhà thuốc Ngọc Anh",
   "address": "Chợ Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.0966345,
   "Latitude": 106.1858408
 },
 {
   "STT": 43,
   "Name": "Nhà thuốc Bãy Do",
   "address": "Khóm 2 TT Vũng Liêm, Tỉnh Vĩnh Long",
   "Longtitude": 10.095438,
   "Latitude": 106.185777
 },
 {
   "STT": 44,
   "Name": "Nhà thuốc Lê văn Út",
   "address": "Chợ Tân Quới, Tỉnh Vĩnh Long",
   "Longtitude": 10.10064,
   "Latitude": 105.7551938
 },
 {
   "STT": 45,
   "Name": "Nhà thuốc Duy Ninh",
   "address": "Số 0497 Tổ 12, Ấp Tân Lộc, Tỉnh Vĩnh Long",
   "Longtitude": 10.150645,
   "Latitude": 105.9729255
 },
 {
   "STT": 46,
   "Name": "Nhà thuốc Thiên Long",
   "address": "Xã Tân Lược, Tỉnh Vĩnh Long",
   "Longtitude": 10.1490834,
   "Latitude": 105.7292491
 },
 {
   "STT": 47,
   "Name": "Nhà thuốc Lê Thành Tân",
   "address": "Chợ Tân Lược, Tỉnh Vĩnh Long",
   "Longtitude": 10.1428244,
   "Latitude": 105.7176778
 },
 {
   "STT": 48,
   "Name": "Nhà thuốc  Lê Thành Minh",
   "address": "Tân Lược, Tỉnh Vĩnh Long",
   "Longtitude": 10.1490834,
   "Latitude": 105.7292491
 },
 {
   "STT": 49,
   "Name": "Nhà thuốc Trang Dung",
   "address": "Tân Quới, Tân Quới, Bình Tân, Vĩnh Long",
   "Longtitude": 10.0962481,
   "Latitude": 105.7556564
 },
 {
   "STT": 50,
   "Name": "Nhà thuốc Ngọc Thi",
   "address": "54 Ngô Quyền, Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0663205,
   "Latitude": 105.8218682
 },
 {
   "STT": 51,
   "Name": "Nhà thuốc Y Lâm Đường",
   "address": "296 khóm 1 , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0665624,
   "Latitude": 105.8084823
 },
 {
   "STT": 52,
   "Name": "Nhà thuốc Bình Minh",
   "address": "108 Phan Văn Năm , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0696001,
   "Latitude": 105.8249692
 },
 {
   "STT": 53,
   "Name": "Nhà thuốc Kim Anh",
   "address": "936 Phan Văn Năm, Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0677663,
   "Latitude": 105.8221733
 },
 {
   "STT": 54,
   "Name": "Nhà thuốc Huỳnh Nga",
   "address": "6142 Không Tên , Cái Vồn , Bình Minh, Vĩnh Long",
   "Longtitude": 10.0665624,
   "Latitude": 105.8084823
 },
 {
   "STT": 55,
   "Name": "Nhà thuốc Mỹ Ngân",
   "address": "Quốc lộ 1A , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0690473,
   "Latitude": 105.8339854
 },
 {
   "STT": 56,
   "Name": "Nhà thuốc Minh Đức",
   "address": "877 Ngô Quyền, Cái Vồn, Bình Minh, Vĩnh Long ",
   "Longtitude": 10.0655526,
   "Latitude": 105.8226112
 },
 {
   "STT": 57,
   "Name": "Nhà thuốc Khánh Nguyên ",
   "address": "Phan Văn Năm , Cái Vồn , Bình Minh, Vĩnh Long",
   "Longtitude": 10.0645485,
   "Latitude": 105.8148633
 },
 {
   "STT": 58,
   "Name": "Nhà thuốc Phương Dung",
   "address": "1/1 Ngô Quyền , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0687811,
   "Latitude": 105.8206877
 },
 {
   "STT": 59,
   "Name": "Nhà thuốc Huỳnh Nga",
   "address": "6142 Phan Văn Năm , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0651954,
   "Latitude": 105.8187032
 },
 {
   "STT": 60,
   "Name": "Nhà thuốc Phương Trang",
   "address": "3734 quốc lộ 1A, Thành Phước, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0610769,
   "Latitude": 105.7999505
 },
 {
   "STT": 61,
   "Name": "Nhà thuốc Hồng Phước",
   "address": "Đường Không Tên, Cái Vồn, Bình Minh, Vĩnh Long ",
   "Longtitude": 10.0686778,
   "Latitude": 105.8207223
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Long Châu",
   "address": "8 Đoàn Thị Điểm, 1, Vĩnh Long, Vĩnh Long ",
   "Longtitude": 10.2559564,
   "Latitude": 105.9724849
 },
 {
   "STT": 63,
   "Name": "Nhà thuốc Thanh Tú ",
   "address": "47 Phan Văn Năm, Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0648456,
   "Latitude": 105.8119302
 },
 {
   "STT": 64,
   "Name": "Nhà thuốc tây Anh Duy",
   "address": "Quốc lộ 1A , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0690473,
   "Latitude": 105.8339854
 },
 {
   "STT": 65,
   "Name": "Nhà thuốc Tôn Sanh Đường",
   "address": "Chợ Cái Vồn, thị trấn Bình Minh , Cái Vồn, Bình Minh, Vĩnh Long",
   "Longtitude": 10.0655526,
   "Latitude": 105.8226112
 }
];