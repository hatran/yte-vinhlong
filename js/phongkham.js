var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám Sản phụ khoa & Siêu âm - Bác sĩ Hồ Thị Thu Hằng",
   "address": "191 Phạm Thái Bường, 4, Vĩnh Long, Vĩnh Long",
   "D": 2703820698,
   "Longtitude": 10.2400707,
   "Latitude": 105.9814901
 },
 {
   "STT": 2,
   "Name": "Phòng khám Đa khoa Mekomed",
   "address": "75 Phạm Thái Bường, 4, Vĩnh Long, Vĩnh Long",
   "D": 2703838911,
   "Longtitude": 10.2449627,
   "Latitude": 105.9779888
 },
 {
   "STT": 3,
   "Name": "Phòng khám Đa khoa Ánh Thủy",
   "address": " 73/21 - Đường Nguyễn Văn Nhung - Khóm 4 - phường 3 - thành phố hường Vĩnh Long ",
   "D": 2703837072,
   "Longtitude": 10.2412179,
   "Latitude": 105.9876019
 },
 {
   "STT": 4,
   "Name": "Phòng khám Nhi Đồng Sài Gòn – Vĩnh Long",
   "address": "56/2 Phạm Thái Bường, phường 4, Vĩnh Long",
   "D": 2703896879,
   "Longtitude": 10.2494388,
   "Latitude": 105.9744192
 },
 {
   "STT": 5,
   "Name": "Phòng khám da liễu và nhi khoa Bác sĩ Phan Văn Năm",
   "address": "33/11B, đường Phạm Thái Bường, phường 4, thành phố hường Vĩnh Long, Vĩnh Long",
   "D": 918210333,
   "Longtitude": 10.2437727,
   "Latitude": 105.9791522
 },
 {
   "STT": 6,
   "Name": "Phòng khám nhi Bác sĩ Cao Thị Phi Nga",
   "address": "140 Nguyễn Thị Minh Khai, phường 1, Vĩnh Long",
   "D": 2703827114,
   "Longtitude": 10.2541088,
   "Latitude": 105.9760416
 },
 {
   "STT": 7,
   "Name": "Phòng khám nhi Bác sĩ Trần Kim Hoàng",
   "address": "25, Đường Võ Thị Sáu, phường 1, Vĩnh Long",
   "D": 2703823544,
   "Longtitude": 10.2517473,
   "Latitude": 105.9701295
 },
 {
   "STT": 8,
   "Name": "Phòng Mạch Bác sĩ Đặng Kim Ngọc",
   "address": "37 Trần Phú, phường 4, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703824113,
   "Longtitude": 10.2510735,
   "Latitude": 105.9785628
 },
 {
   "STT": 9,
   "Name": "Phòng Mạch Bác sĩ Đoàn Hùng",
   "address": "66B Đinh Tiên Hoàng, phường 8, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703827207,
   "Longtitude": 10.2314992,
   "Latitude": 105.9557104
 },
 {
   "STT": 10,
   "Name": "Phòng Mạch Bác sĩ Huỳnh Văn Long",
   "address": "78B Đường 2 Tháng 9, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703824023,
   "Longtitude": 10.239574,
   "Latitude": 105.9571928
 },
 {
   "STT": 11,
   "Name": "Phòng Mạch Bác sĩ Lê Kim Hồng",
   "address": "153 Ấp Tân Thuận An, xã Tân Ngãi, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703822874,
   "Longtitude": 10.2713517,
   "Latitude": 105.912784
 },
 {
   "STT": 12,
   "Name": "Phòng Mạch Bác sĩ Lê Minh Huân",
   "address": " 249/4/3 Phạm Thái Bường, phường 4, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703825099,
   "Longtitude": 10.2460306,
   "Latitude": 105.9771986
 },
 {
   "STT": 13,
   "Name": "Phòng Mạch Bác sĩ Lưu Thành Đoàn",
   "address": "Ấp Phước Nhơn, xã Phước Hậu, huyện Long Hồ, Vĩnh Long",
   "D": 2703823026,
   "Longtitude": 10.2163296,
   "Latitude": 105.9611748
 },
 {
   "STT": 14,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Đức Hùng",
   "address": "73 Trần Văn Ơn, phường 1, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703822287,
   "Longtitude": 10.2538012,
   "Latitude": 105.9693186
 },
 {
   "STT": 15,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Ngọc Thái",
   "address": "69/55 Nguyễn Huệ, phường 2, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703828034,
   "Longtitude": 10.253104,
   "Latitude": 105.9626995
 },
 {
   "STT": 16,
   "Name": "Phòng Mạch Bác sĩ Phạm Cao Thắng",
   "address": "78/2/2 Trần Văn Ơn, phường 1, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703824327,
   "Longtitude": 10.2543271,
   "Latitude": 105.9687464
 },
 {
   "STT": 17,
   "Name": "Phòng Mạch Bác sĩ Phạm Minh Thanh ",
   "address": "264/23 Phạm Hùng, phường 9, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703824375,
   "Longtitude": 10.2552276,
   "Latitude": 105.9623283
 },
 {
   "STT": 18,
   "Name": "Phòng Mạch Bác sĩ Phong Tuấn",
   "address": "111/6 Phạm Thái Bường, phường 4, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703852313,
   "Longtitude": 10.2494369,
   "Latitude": 105.9744168
 },
 {
   "STT": 19,
   "Name": "Phòng mạch - Bác sĩ Tường Thụy",
   "address": "Khóm 1 thị trấn, Cái Vồn, Bình Minh, Vĩnh Long",
   "D": 2703890103,
   "Longtitude": 10.0672385,
   "Latitude": 105.8079673
 },
 {
   "STT": 20,
   "Name": "Phòng Mạch Bác sĩ Dương Văn Cù",
   "address": "64/17B Trần Phú, phường 4, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703822062,
   "Longtitude": 10.2514483,
   "Latitude": 105.9782463
 },
 {
   "STT": 21,
   "Name": "Phòng Mạch Bác sĩ Lê Hoàng Trinh",
   "address": "Ấp Hiếu Xuân Tây, xã Hiếu Thành, huyện Vũng Liêm, Vĩnh Long",
   "D": 2703889758,
   "Longtitude": 9.9897411,
   "Latitude": 106.1051705
 },
 {
   "STT": 22,
   "Name": "Phòng Mạch Bác sĩ Lê Thị Kiên",
   "address": "KP Hựu Thành xã Hựu Thành, huyện Trà Ôn, Vĩnh Long",
   "D": 2703889855,
   "Longtitude": 9.9823916,
   "Latitude": 106.0346285
 },
 {
   "STT": 23,
   "Name": "Phòng Mạch Lương Y Nguyễn Bá Dũng",
   "address": "Ấp Chợ, xã Mỹ An, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703939002,
   "Longtitude": 10.2310542,
   "Latitude": 106.0463837
 },
 {
   "STT": 24,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Hoàng Mai",
   "address": "8/1A Khu 1 Lê Văn Duyệt, thị trấn Trà Ôn, huyện Trà Ôn, Vĩnh Long",
   "D": 2703770549,
   "Longtitude": 9.9655246,
   "Latitude": 105.9206658
 },
 {
   "STT": 25,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Hữu Loạt",
   "address": "Ấp Tường Trí, xã Tường Lộc, huyện Tam Bình, Vĩnh Long",
   "D": 2703713888,
   "Longtitude": 10.0570612,
   "Latitude": 106.0023054
 },
 {
   "STT": 26,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Quang Phụng",
   "address": " 4A Khu 1 Gia Long, thị trấn Trà Ôn, huyện Trà Ôn, Vĩnh Long",
   "D": 2703770220,
   "Longtitude": 9.9663164,
   "Latitude": 105.9217353
 },
 {
   "STT": 27,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Văn Sơn",
   "address": "28/2A Khu 8 thị trấn Trà Ôn, huyện Trà Ôn, Vĩnh Long",
   "D": 2703770703,
   "Longtitude": 9.964089,
   "Latitude": 105.923644
 },
 {
   "STT": 28,
   "Name": "Phòng Mạch Bác sĩ Hậu",
   "address": "Khóm 1 thị trấn Cái Nhum, huyện Mang Thít, Vĩnh Long",
   "D": 2703840555,
   "Longtitude": 10.1740716,
   "Latitude": 106.1095335
 },
 {
   "STT": 29,
   "Name": "Phòng Mạch Bác sĩ Lê Thanh Châu",
   "address": "147 K2 xã Tân Hội, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703823708,
   "Longtitude": 10.263513,
   "Latitude": 105.885683
 },
 {
   "STT": 30,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Văn Hinh",
   "address": "12/26 Kthị trấn Trường THYT phường 4, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703816033,
   "Longtitude": 10.2448442,
   "Latitude": 105.958865
 },
 {
   "STT": 31,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Văn Lộc",
   "address": "66 Phạm Thái Bường, phường 4, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703825068,
   "Longtitude": 10.2447184,
   "Latitude": 105.977939
 },
 {
   "STT": 32,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Văn Minh",
   "address": "46/12 Trần Văn Ơn, phường 1, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703823617,
   "Longtitude": 10.2543271,
   "Latitude": 105.9687464
 },
 {
   "STT": 33,
   "Name": "Phòng Mạch Bác sĩ Thuấn",
   "address": "Khóm 2 thị trấn Tam Bình, huyện Tam Bình, Vĩnh Long",
   "D": 2703860798,
   "Longtitude": 10.0478007,
   "Latitude": 105.9978981
 },
 {
   "STT": 34,
   "Name": "Phòng Mạch Bác sĩ Trần Kim Chi",
   "address": "Ấp An Hương 2, xã Mỹ An, huyện Mang Thít, Vĩnh Long",
   "D": 2703849823,
   "Longtitude": 10.2433664,
   "Latitude": 106.0387408
 },
 {
   "STT": 35,
   "Name": "Phòng Mạch Bác sĩ Trần Kim Hoàng",
   "address": "25 Võ Thị Sáu, phường 1, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703823544,
   "Longtitude": 10.2517473,
   "Latitude": 105.9701295
 },
 {
   "STT": 36,
   "Name": "Phòng Mạch Bác sĩ Trần Minh Đức",
   "address": "62 Lý Thường Kiệt, phường 1, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703824713,
   "Longtitude": 10.2528736,
   "Latitude": 105.9761939
 },
 {
   "STT": 37,
   "Name": "Phòng Mạch Bác sĩ Trịnh Thị Kim Hồng",
   "address": "E/2 Cc Biệt Thự Ngọc Vân Phó Cơ Điều, phường 3, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703824130,
   "Longtitude": 10.2342982,
   "Latitude": 105.9690439
 },
 {
   "STT": 38,
   "Name": "Phòng Mạch Bác sĩ Trường Giang",
   "address": "Khóm 1 thị trấn Tam Bình, huyện Tam Bình, Vĩnh Long",
   "D": 2703860210,
   "Longtitude": 10.0478007,
   "Latitude": 105.9978981
 },
 {
   "STT": 39,
   "Name": "Phòng Mạch Bác sĩ Nguyễn Mười Hai",
   "address": "A18/12/26 Phạm Thái Bường, phường 4, thị xã Vĩnh Long, Vĩnh Long",
   "D": 2703827179,
   "Longtitude": 10.2493526,
   "Latitude": 105.9746468
 },
 {
   "STT": 40,
   "Name": "Phòng Mạch Bác sĩ Tô Hồng Huyện",
   "address": "24/3 Khu 1 Gia Long, thị trấn Trà Ôn, huyện Trà Ôn, Vĩnh Long",
   "D": 2703770657,
   "Longtitude": 9.964089,
   "Latitude": 105.923644
 },
 {
   "STT": 41,
   "Name": "Phòng Mạch Bác sĩ Trần Văn Ba",
   "address": "82/75 Khu 4 Trưng Trắc, huyện Trà Ôn, Vĩnh Long",
   "D": 2703770953,
   "Longtitude": 9.9623874,
   "Latitude": 105.921201
 },
 {
   "STT": 42,
   "Name": "Phòng Mạch Bác sĩ Trần Văn Hiệp",
   "address": "18 Khu 2 Thống Chế Điều Bát, huyện Trà Ôn, Vĩnh Long",
   "D": 2703770043,
   "Longtitude": 9.9629428,
   "Latitude": 105.9268236
 },
 {
   "STT": 43,
   "Name": "Phòng Mạch Bác sĩ Trung",
   "address": "Xã Hiếu Phụng, huyện Vũng Liêm, Vĩnh Long",
   "D": 2703874388,
   "Longtitude": 10.0865819,
   "Latitude": 106.1169299
 },
 {
   "STT": 44,
   "Name": "Phòng Mạch Bác sĩ Út",
   "address": "Khóm 1 thị trấn Cái Nhum, huyện Mang Thít, Vĩnh Long",
   "D": 2703840990,
   "Longtitude": 10.1740716,
   "Latitude": 106.1095335
 }
];