var datakhoidonvikhac = [
  {
   "STT": 1,
   "Name": "Công ty TNHH Dược Phẩm Tây Nam",
   "address": "79 , Đường 1/5 ,  thành phố  Vĩnh Long",
   "Longtitude": 10.2688361,
   "Latitude": 105.9102952
 },
 {
   "STT": 2,
   "Name": "Công ty TNHH Dược Phẩm Khả Thy",
   "address": "64/4C Trần Phú ,  thành phố  Vĩnh Long",
   "Longtitude": 10.251497,
   "Latitude": 105.9779951
 },
 {
   "STT": 3,
   "Name": "Nhà thuốc Hồng Huệ",
   "address": "18 , đường 1/5 ,  thành phố  Vĩnh Long",
   "Longtitude": 10.2259191,
   "Latitude": 105.9349072
 },
 {
   "STT": 4,
   "Name": "Công ty TNHH Dược Phẩm Phú Hải",
   "address": "67/3 ,  Phạm Thái Bường ,  thành phố  Vĩnh Long",
   "Longtitude": 10.249529,
   "Latitude": 105.9745589
 },
 {
   "STT": 5,
   "Name": "Công Ty Cổ Phần Dược Phẩm Agimexphar M",
   "address": "26, đường 1/5, thành phố  Vĩnh Long",
   "Longtitude": 10.2663842,
   "Latitude": 105.9219222
 },
 {
   "STT": 6,
   "Name": "Công ty TNHH Dược Phẩm Liên Thành",
   "address": "Phạm Hùng , P9, thành phố  Vĩnh Long",
   "Longtitude": 10.2594784,
   "Latitude": 105.9458963
 },
 {
   "STT": 7,
   "Name": "Công ty TNHH Dược Phẩm Trúc Chi",
   "address": "4 ,  Chi Lăng, thành phố  Vĩnh Long",
   "Longtitude": 10.2571177,
   "Latitude": 105.9730428
 },
 {
   "STT": 8,
   "Name": "Công ty TNHH Dược Phẩm Minh Hạnh",
   "address": "78 , Trần Phú , P 4, thành phố  Vĩnh Long",
   "Longtitude": 10.2303754,
   "Latitude": 105.9920484
 },
 {
   "STT": 9,
   "Name": "Chi nhánh Công ty CP Dược phẩm Cửu Long tại tỉnh An",
   "address": "5 ,  Đinh Tiên Hoàng, thành phố  Vĩnh Long",
   "Longtitude": 10.2396886,
   "Latitude": 105.9572055
 },
 {
   "STT": 10,
   "Name": "Chi nhánh Công ty CP Dược Hậu Giang tại Tp.Long Xuyên",
   "address": "Cao Thắng , P 8 , thành phố  Vĩnh Long",
   "Longtitude": 10.2370022,
   "Latitude": 105.959157
 },
 {
   "STT": 11,
   "Name": "Công Ty TNHH Một Thành Viên Dược Phẩm Huy Thảo",
   "address": "Phạm Hùng, P 9, thành phố  Vĩnh Long",
   "Longtitude": 10.2594784,
   "Latitude": 105.9458963
 }
];